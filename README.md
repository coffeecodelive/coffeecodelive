## COFFEE CODE LIVE
https://coffeecode.live

Secure personal productivity monitoring

Requies Python3 and Windows OS

How to get started:
Install Python 3
Also install Win32API via:
*pip3 install pywin32*

From there, just run the script:
*python coffee.py*

After a few seconds, copy/paste the link provided in the console display into your browser.
Leave the application run while you want to be monitored.

Control-C to cancel and close the app.

The online dashboard at https://coffeecode.live will showcase your last 14 hours of activiity
No passwords are needed; just the link. The data is made anonymous, so no password is needed.
The account is auto-created and is linked to the computer you are running the script on.

-happy computing.