# designed for Python 3 ; tested on Windows
# coded by steve seguin

# DEPENDENCIES NEEDED:
# pip3 install pywin32

import subprocess
import hashlib
import requests
import json
import threading

from win32gui import GetWindowText, GetForegroundWindow, EnumWindows
from ctypes import Structure, windll, c_uint, sizeof, byref

global uuid
m = hashlib.sha224() #meh
uuid = subprocess.check_output('wmic csproduct get UUID') # get unique PC id
m.update(uuid) # create hash
uuid = int(hashlib.sha1(uuid).hexdigest(), 16) % (10 ** 10) ## reduce the hash
print("Your user id is",uuid)


class LASTINPUTINFO(Structure):
    _fields_ = [
        ('cbSize', c_uint),
        ('dwTime', c_uint),
    ]
def get_idle_duration():
    lastInputInfo = LASTINPUTINFO()
    lastInputInfo.cbSize = sizeof(lastInputInfo)
    windll.user32.GetLastInputInfo(byref(lastInputInfo))
    millis = windll.kernel32.GetTickCount() - lastInputInfo.dwTime
    return int(millis / 1000.0)

def windowEnumerationHandler(hwnd, top_windows):
    top_windows.append((hwnd, GetWindowText(hwnd)))

def checkState():
	top_windows = []
	forground_id = GetForegroundWindow()
	active_windows = [(True, [forground_id,GetWindowText(forground_id)])]
	EnumWindows(windowEnumerationHandler, top_windows)
	for i in top_windows:
		if "google chrome" in i[1].lower():
			active_windows.append((False,i))
		continue

	### LINUX FRIENDLY, but needs some more work
	# for i in psutil.pids(): # Print all pids
		# try:
			# p = psutil.Process(i)  # The pid of desired process
			# print(p.name()) # If the name is "python.exe" is called by python
			# print(p.cmdline()) # Is the command line this process has been called with
		# except:
			# pass
	#for proc in psutil.win_service_iter():
	#	 sinfo = proc.as_dict()
	#	 break
		 #print("[*] Name: {} Display name: {} Status: {} Type: {} Path: ".format(sinfo["name"], sinfo["display_name"], sinfo["status"], sinfo["start_type"], sinfo["binpath"]))
	##################

	r = requests.post("https://coffeecode.live/post.php", data={'uuid': uuid, 'name': json.dumps(active_windows), 'idle': get_idle_duration()})
	print(r.status_code, r.reason)
	print(r.text)
	return True
	
def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

try:
	response = checkState
	if (response):
		print("Monitoring has started successfully")
	else:
		print("Something failed; connection perhaps?")
except:
	print("This script had errors. Please correct")
	exit()
	
set_interval(checkState, 5)